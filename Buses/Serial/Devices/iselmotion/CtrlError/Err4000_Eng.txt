;**************************************************************
;Error messages to be used by CheckErr.DLL
;This text file contains error messages that may be modified
;by the user for own purposes, e.g. to show specific error messages
;when an external error signal occurs at a specific input.
;(c) isel Automation KG 2008. All rights reserved.
;**************************************************************
;Facility: 4000 - FAC_USERSPECIFIC

;Kein Fehler
0-4000-0: No error!

;Informationen
1-4000-1: Informational text (may be modified)!

;Warnings
2-4000-1: Warning text (may be modified)!

;Fehler
3-4000-1: Signalization interface: The external error 1 occured!
3-4000-2: Signalization interface: The external error 1 occured!
3-4000-3: Signalization interface: The external error 1 occured!
3-4000-4: Signalization interface: The external error 1 occured!

;//////////////////////////////////////////////////////////////
;End Of File
