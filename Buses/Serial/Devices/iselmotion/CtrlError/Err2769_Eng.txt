;**************************************************************
;Error messages for CheckErr.DLL
;Do not modify this text file!
;(c) isel Automation KG 1999. All rights reserved.
;**************************************************************
;Facility: FAC_NCCTRL

;//////////////////////////////////////////////////////////////
;Specific section

0-2769-0: No error!

;//////////////////////////////////////////////////////////////
;Common Section

3-2769-4097: Could not find the assigned module DLL!
3-2769-4098: Could not find essential initialization data of NCCTRL.DLL!
3-2769-4099: Incorrect version of the assigned module DLL!
3-2769-4100: Invalid module index!
3-2769-4101: The accessed module is disabled!
3-2769-4102: The accessed module is not yet initialized!
3-2769-4103: The accessed module is busy with another function call!
3-2769-4369: Could not find/locate the initialization file NCCTRL.INI!
3-2769-4370: Could not open the initialization file NCCTRL.INI!
3-2769-4371: Could not read from the initialization file NCCTRL.INI!
3-2769-4372: Could not write to initialization file NCCTRL.INI!
3-2769-4373: Incorrect version of the initalization file NCCTRL.INI!
3-2769-4386: Could not open/create a temporary file!
3-2769-4387: Could not read from a temporary file!
3-2769-4388: Could not write to a temporary file!
3-2769-4401: Invalid file name!
3-2769-4609: User break! The last executed operation was aborted by user request.
3-2769-4641: Could not start a worker thread!
3-2769-4642: An invalid index value was used!
3-2769-4643: Erroneous or invalid data occured!
3-2769-4644: An invalid position was given to the control resp. erroneous positions occured!
3-2769-4645: A process variable could not be created!
3-2769-4646: Unknown function: Either the requested function is unknown within the module DLL or the version of the used DLL is too old!
3-2769-4647: NULL Pointer: A invalid pointer was given!
3-2769-4648: Invalid velocity: The given value is either negative or too big!
3-2769-4649: Invalid speed: The given value is either negative or too big!
3-2769-4650: Invalid port number: The given value is either negative or too big!
3-2769-4651: Invalid bit number: The given value is either negative or greater than 7!
3-2769-65535: Unknown error!

;//////////////////////////////////////////////////////////////
;End Of File
