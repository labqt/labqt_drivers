;**************************************************************
;Error messages for CheckErr.DLL
;Do not modify this text file!
;(c) isel Automation KG 1999. All rights reserved.
;**************************************************************
;Facility: FAC_IML4CTRL

0-2316-0: Kein Fehler!

;//////////////////////////////////////////////////////////////
;Specific section

;Warnings

;Errors
3-2316-1: 1: Fehler in �bergebener Zahl!
3-2316-2: 2: Ein oder mehrere Endschalter sind aktiv, Reset und Referenzfahrt sind erforderlich!
3-2316-3: 3: Unzul�ssige Achsangabe!
3-2316-4: 4: Es sind noch keine Achsen definiert, Reset erforderlich!
3-2316-5: 5: Syntax-Fehler, unbekannter oder fehlerhaftes Kommando!
3-2316-6: 6: Das Ende des internen Steuerungsspeichers ist erreicht!
3-2316-7: 7: Unzul�ssige Parameteranzahl!
3-2316-8: 8: Zu speichernder Befehl ist inkorrekt!
3-2316-9: 9: Allgemeiner Anlagenfehler (Keine Spannung, NOT-Aus aktiv, Haube ist offen, ...)!
3-2316-10: A: Fehlercode von Steuerung IML4 nicht benutzt!
3-2316-11: B: Fehlercode von Steuerung IML4 nicht benutzt!
3-2316-12: C: CR erwartet! Die Steuerung hat als Befehlsabschlu� ein CR (Carriage Return) erwartet, aber nicht bekommen.
3-2316-13: D: Unzul�ssige Geschwindigkeit!
3-2316-14: E: Fehlercode von Steuerung IML4 nicht benutzt!
3-2316-15: F: Benutzerstop aktiv!
3-2316-16: G: Ein ung�ltiges Datenfeld wurde �bergeben!
3-2316-17: H: Befehl ist bei ge�ffneter Haube nicht ausf�hrbar!
3-2316-18: =: Fehlercode von Steuerung IML4 nicht benutzt!

;//////////////////////////////////////////////////////////////
;Common Section

;Warnings

;Errors

;//////////////////////////////////////////////////////////////
;End Of File
