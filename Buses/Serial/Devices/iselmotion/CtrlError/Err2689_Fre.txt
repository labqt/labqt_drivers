;**************************************************************
;Error messages for CheckErr.DLL
;Do not modify this text file!
;(c) isel Automation KG 1999. All rights reserved.
;**************************************************************
;Facility: FAC_MCTLADM

0-2689-0: Aucune erreur!

;//////////////////////////////////////////////////////////////
;Specific section


;//////////////////////////////////////////////////////////////
;Common Section

;Warnings
2-2689-4101: Le module de commande de mouvement choisi  est d�sactiv�!  La fonction demand�e n'a pas pu �tre ex�cut�e!

;Errors
3-2689-1: Num�ro de port invalide! Le port est invalide ou n'est pas dans une gamme valide.
3-2689-2: Num�ro de bit invalide! Le bit donn�e est invalide ou n'est pas dans une gamme valide.
3-2689-3: Port d�sactiv�! Le port requis est d�sactiv� et inaccessible.

3-2689-7: L'outils courant est inconnue! L'�tat de l'outil courant est inconue, donc il n'est pas possible d'�x�cuter la fonction requise, ex. la correction en Z du point zero.
3-2689-8: R�f�rence outil inconue! L'outil utiliser pour d�finir le point zero est inconnue.
3-2689-9: Le cone est vide! Aucun outils dans la pince, donc il n'est pas possible d'�x�cuter la fonction requise, ex. la correction en Z du point zero.
3-2689-10: Longueur d'outil inconnue! La longueur de l'outil dans le cone est inconnue! donc il n'est pas possible d'�x�cuter la fonction requise, ex. la correction en Z du point zero.
3-2689-11: R�f�rence outil inconnue! L'outil utiliser pour d�finir le point zero est inconnue, donc il n'est pas possible d'�x�cuter la fonction requise, ex. la correction en Z du point zero.
3-2689-12: R�f�rence outil inconnue! L'outil utiliser pour d�finir le point zero est inconnue, donc il n'est pas possible d'�x�cuter la fonction requise, ex. la correction en Z du point zero.

3-2689-4097: Impossible de trouver la DLL de commande de mouvement!
3-2689-4098: Impossible de trouver les donn�es essentielles d'initialisation!
3-2689-4099: Version incorrect de la DLL de d�placement ! La fonction demand�e ne peut �tre executer!
3-2689-4100: Index de module invalide! Le module de commande de mouvement demand� n'existe pas !
3-2689-4101: Le module de commande de mouvement est d�sactiv�, La fonction demand�e ne peut �tre execut�e!
3-2689-4102: Le module de commande de mouvement n'est pas encore initialis�!
3-2689-4103: Le module de commande de mouvement est occup� par un autre appel de fonction!
3-2689-4369: Impossible de trouver le fichier d'initialisation NCCTRL.INI!
3-2689-4370: Impossible d'ouvrir le fichier d'initialisation NCCTRL.INI!
3-2689-4371: Impossible de lire le fichier d'initialisation NCCTRL.INI!
3-2689-4372: Impossible d'�crire dans le fichier d'initialisation NCCTRL.INI!
3-2689-4373: Version incorrect du fichier d'initialisation NCCTRL.INI!
3-2689-4386: Impossible de cr�er/d'ouvrir un fichier temporaire!
3-2689-4387: Impossible de lire dans un fichier temporaire!
3-2689-4388: Impossible d'�crire dans un fichier temporaire!
3-2689-4401: Nom de fichier invalide!
3-2689-4609: Coupure d'utilisateur ! La derni�re action ex�cut�e a �t� avort�e � la demande de l'utilisateur.
3-2689-4641: Impossible de d�marrer un thread!
3-2689-4642: Une valeur d'index invalide a �t� utilis�e!
3-2689-4643: Des donn�es incorrectes ou invalide se sont produites!
3-2689-4644: Une position invalide a �t� donn� au control resp. Une erreur de position s'est produite!
3-2689-4645: Une variable de processus n'a pas pu �tre cr�e!
3-2689-4646: Function inconnue : L'une des fonctions demand�es est inconnue dans la DLL o� la version utilis�e est trop ancienne !
3-2689-4647: Pointer nul : Un pointeur invalide a �t� donn� !
3-2689-4648: Vitesse invalide : La valeur est soit n�gative, soit trop grande !
3-2689-4649: Vitesse invalide : La valeur est soit n�gative, soit trop grande !
3-2689-4650: Num�ro de port invalide : La valeur est soit n�gative, soit trop grande !
3-2689-4651: Nombre de bit invalide : La valeur est soit n�gative, soit sup�rieur � 7!
3-2689-65535: Erreur inconnue!

;//////////////////////////////////////////////////////////////
;End Of File
