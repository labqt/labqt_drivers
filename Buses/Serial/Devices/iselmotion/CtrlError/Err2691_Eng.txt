;**************************************************************
;Error messages for CheckErr.DLL
;Do not modify this text file!
;(c) isel Automation KG 1999. All rights reserved.
;**************************************************************
;Facility: FAC_IOADM

0-2691-0: No error!

;//////////////////////////////////////////////////////////////
;Specific section

3-2691-1: Invalid logical port number! The given port number is not in a valid range within the IO administration.
3-2691-2: Invalid logical bit number! The given bit number is not in a valid range within the IO administration.
3-2691-3: Port is disabled! The required port is disabled within the IO administration.

3-2691-4: Invalid logical system port number! The given system port number is not in a valid range within the IO administration.
3-2691-5: Invalid logical bit number! The given bit number is not in a valid range.
3-2691-6: System port is disabled! The required system port is disabled within the IO administration.

3-2691-17: Required input is inactive! The required user input was not set resp. activated within the IO administration.
3-2691-18: Required output is inactive! The required user output was not set resp. activated within the IO administration.
3-2691-19: Required system input is inactive! The required system input was not set resp. activated within the IO administration.
3-2691-20: Required system output is inactive! The required system output was not set resp. activated within the IO administration.

3-2691-33: Error during periphery access! The required peripheral device could not be accessed correctly. Please check the configuration of the peripheral devices.
3-2691-34: Error during initialization of a periphery output channel! The initialization of an analog output channel for accessing a peripheral device could not be executed correctly. Please check the configuration of the peripheral devices.

3-2691-49: Invalid number of an analog channel! An analog input or output channel with the given number does not exist.
3-2691-50: Required analog channel inactive! An analog input or output channel with the given number is inactive or was not set correctly in the IO administration.

;//////////////////////////////////////////////////////////////
;Common Section

;Warnings
2-2691-4101: The accessed IO module is disabled! The requested function could not be executed.

;Errors
3-2691-4097: Could not find the assigned IO DLL!                                                    
3-2691-4098: Could not find essential initialization data!                                               
3-2691-4099: Incorrect version of the accessed IO DLL! The requested function could not be executed.
3-2691-4100: Invalid module index! The requested IO module does not exist.                          
3-2691-4101: The accessed IO module is disabled, could not execute the requested function!          
3-2691-4102: The accessed IO module is not yet initialized!                                         
3-2691-4103: The accessed IO module is busy with another function call!                             
3-2691-4369: Could not find/locate the initialization file NCCTRL.INI!                                   
3-2691-4370: Could not open the initialization file NCCTRL.INI!                                          
3-2691-4371: Could not read from the initialization file NCCTRL.INI!                                     
3-2691-4372: Could not write to initialization file NCCTRL.INI!                                          
3-2691-4373: Incorrect version of the initalization file NCCTRL.INI!                                     
3-2691-4386: Could not open/create a temporary file!                                                     
3-2691-4387: Could not read from a temporary file!                                                       
3-2691-4388: Could not write to a temporary file!                                                        
3-2691-4401: Invalid file name!                                                                          
3-2691-4609: User break! The last executed operation was aborted by user request.                        
3-2691-4641: Could not start a worker thread!                                                            
3-2691-4642: An invalid index value was used!
3-2691-4643: Erroneous or invalid data occured!
3-2691-4644: An invalid position was given to the control resp. erroneous positions occured!
3-2691-4645: A process variable could not be created!
3-2691-4646: Unknown function: Either the requested function is unknown within the module DLL or the version of the used DLL is too old!
3-2691-4647: NULL Pointer: A invalid pointer was given!
3-2691-4648: Invalid velocity: The given value is either negative or too big!
3-2691-4649: Invalid speed: The given value is either negative or too big!
3-2691-4650: Invalid port number: The given value is either negative or too big!
3-2691-4651: Invalid bit number: The given value is either negative or greater than 7!
3-2691-65535: Unknown error!                                                                             

;//////////////////////////////////////////////////////////////
;End Of File
