;**************************************************************
;Error messages for CheckErr.DLL
;Do not modify this text file!
;(c) isel Automation KG 2002. All rights reserved.
;**************************************************************
;Facility: FAC_PRONC

;//////////////////////////////////////////////////////////////
;Specific section

3-3074-1: There is no workpiece zero point declared in the list of machine positions!
3-3074-2: There is no parking position declared in the list of machine positions!
3-3074-3: There is no home position declared in the list of machine positions!
3-3074-4: There is no toolchange position declared in the list of machine positions!
3-3074-5: There is no start position declared in the list of machine positions!
3-3074-6: There is no end position declared in the list of machine positions!
3-3074-17: A spindle error has occured! Processing of the current CNC file will be stopped. You can try to correct the spindle error and continue processing.
3-3074-18: An position lag error occured! Processing of the current CNC file will be aborted. Either decrease the processing velocity or check the axis settings of the concerning axis.


;//////////////////////////////////////////////////////////////
;Common Section

3-3074-4098: Could not find essential initialization data!
3-3074-4102: A required DLL was not yet initialized!
3-3074-4103: A required DLL is busy executing another function!
3-3074-4369: Could not find the given initialization file!
3-3074-4370: Could not open the given initialization file!
3-3074-4371: Could not read from the given initialization file!
3-3074-4372: Could not write to the given initialization file!
3-3074-4373: Incorrect version of the given initialization file!
3-3074-4386: Could not create/open a temporary file!
3-3074-4387: Could not read from a temporary file!
3-3074-4388: Could not write to a temporary file!
3-3074-4401: Invalid file name!
3-3074-4609: Aborted by user!
3-3074-4610: Hardware error!
3-3074-4611: Power supply error!
3-3074-4625: Module is locked by security circuit!
3-3074-4641: Could not start a worker thread!
3-3074-4642: An invalid index value was used!
3-3074-4643: Erroneous or invalid data occured!
3-3074-4644: An invalid position was given to the control resp. erroneous positions occured!
3-3074-4645: A process variable could not be created!
3-3074-4646: Unknown function: Either the requested function is unknown within the module DLL or the version of the used DLL is too old!
3-3074-4647: NULL Pointer: A invalid pointer was given!
3-3074-4648: Invalid velocity: The given value is either negative or too big!
3-3074-4649: Invalid speed: The given value is either negative or too big!
3-3074-4650: Invalid port number: The given value is either negative or too big!
3-3074-4651: Invalid bit number: The given value is either negative or greater than 7!
3-3074-4865: RS232 error! Could not connect to interface card.
3-3074-4866: RS232 error! Could not open serial port.
3-3074-4867: RS232 error! Could not set communication parameters.
3-3074-4868: RS232 error! Could not enable serial port!
3-3074-4869: RS232 error! Missing serial connection to interface card IFC5.
3-3074-4870: RS232 error! An error occured while sending data.
3-3074-4871: RS232 error! An error occured while receiving data.
3-3074-4872: RS232 error! A timeout error occured during sending of data.
3-3074-5121: The hood / security door of the machine is open!\n\nPlease close the hood / security door of the machine and check if the locking mechanism is securely locked.
3-3074-5122: The cover of the machine is closed!
3-3074-5137: The spindle is still switched on!
3-3074-5138: The spindle is switched off!
3-3074-5139: Could not set the requested turn direction!
3-3074-5153: The machines axes are not at their home position!
3-3074-5154: The machines axes are at their home position!
3-3074-5155: The machines axes are not at their park position!
3-3074-5156: The machines axes are at their park position!
3-3074-65535: Unknown error!

;//////////////////////////////////////////////////////////////
;End Of File

