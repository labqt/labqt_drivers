;**************************************************************
;Error messages for CheckErr.DLL
;Do not modify this text file!
;(c) isel Automation KG 2002. All rights reserved.
;**************************************************************
;Facility: FAC_IFC5CTRL

0-2311-0: Kein Fehler!

;//////////////////////////////////////////////////////////////
;Specific section

;Warnings

;Errors
3-2311-1: 1: Fehler in �bergebener Zahl!
3-2311-2: 2: Mindestens ein Hardware-Endschalter ist aktiv. F�hren Sie eine Referenzfahrt aus oder w�hlen sie die Funktion "Hardware-Endschalter freifahren" um den Fehler zu beheben!
3-2311-3: 3: Unzul�ssige Achsangabe!
3-2311-4: 4: Es sind noch keine Achsen definiert, Reset erforderlich!
3-2311-5: 5: Syntax-Fehler, unbekannter oder fehlerhaftes Kommando!
3-2311-6: 6: Das Ende des internen Steuerungsspeichers ist erreicht!
3-2311-7: 7: Unzul�ssige Parameteranzahl!
3-2311-8: 8: Zu speichernder Befehl inkorrekt oder Betriebsspannung fehlt! Entweder existiert der zuletzt �bertragene Befehl nicht als CNC-Befehl oder die Spannungsversorgung der Endstufen ist nicht eingeschaltet.
3-2311-9: 9: Fehlercode wird von der Interfacekarte 5 nicht benutzt!
3-2311-10: A: Impulsfehler: Die �bergebene Option f�r den Befehl Impuls ist nicht im Bereich 1 bis 6!
3-2311-11: B: Tell-Fehler: Die Tell-Funktion (starte zweite Interface-Karte) hat kein Endezeichen nach der H�chstzahl der zu sendenden Zeichen gefunden. Dieser Fehler deutet auf Speicherprobleme hin, da die Eingabe des Tell-Befehles immer ein Endezeichen anf�gt.
3-2311-12: C: CR erwartet: Die Interface-Karte hat auf das (CR)-Zeichen als Befehlsende gewartet. Sie haben jedoch ein anderes Zeichen �bergeben. Dies ist haupts�chlich ein Problem der Parameteranzahl.
3-2311-13: D: Unzul�ssige Geschwindigkeit! Auch f�r Achsen ohne Bewegung wird eine g�ltige Geschwindigkeit ben�tigt.
3-2311-14: E: Schleifenfehler! Es wurde versucht, eine Vorw�rtsschleife auszuf�hren. Beachten Sie, dass bei Schleifen immer die letzten n-Befehle wiederholt werden, d.h. 34,4 ist unzul�ssig.
3-2311-15: F: Benutzerstop aktiv!
3-2311-16: G: Ein ung�ltiges Datenfeld wurde �bergeben!
3-2311-17: H: Fehlercode wird von der Interfacekarte 5 nicht benutzt!
3-2311-18: =: Fehlercode wird von der Interfacekarte 5 nicht benutzt!

;//////////////////////////////////////////////////////////////
;Common Section

;Warnings

;Errors

;//////////////////////////////////////////////////////////////
;End Of File
