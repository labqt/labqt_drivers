;**************************************************************
;Error messages for CheckErr.DLL
;Do not modify this text file!
;(c) isel Automation KG 2001. All rights reserved.
;**************************************************************
;Facility: FAC_PARPORT_CAN_WDM

0-2320-0: Aucune erreur!

;//////////////////////////////////////////////////////////////
;Specific section

;Warnings
							  
;Errors
3-2320-1: Impossible de trouv� un bus ISA ou EISA!
3-2320-2: Pas assez de m�moire disponible!
3-2320-3: Le pilote de base (CANPAR.SYS) n'est pas disponible !
3-2320-4: Il y a un conflit mat�riel !
3-2320-5: Le pilote de base du port parall�le (CANPAR.SYS) n'existe pas !
3-2320-6: Le pilote de base du port parall�le (CANPAR.SYS) n'est pas disponible !
3-2320-7: Erreur pendant la connection aux int�ruptions du  port parall�le!
3-2320-8: Le dongle mat�riel CAN est endomag� !
3-2320-9: La d�connection des int�ruptions du port parall�le est impossible!
3-2320-10: Le dongle CAN n'est pas encore initialis� !
3-2320-11: Param�tre d'initialisation du dongle CAN Incorrect !
3-2320-12: Le dongle CAN est d�j� initialis� !
3-2320-13: Nombre d'handle fichier incorrect !
3-2320-14: Param�tres incorrects pour l'interpolation prot�g�!
3-2320-15: Le controlleur CAN est d�connecter du bus !
3-2320-16: La mesure de temps de latence est inactif!


;//////////////////////////////////////////////////////////////
;Common Section

;Warnings

;Errors


;//////////////////////////////////////////////////////////////
;End Of File
