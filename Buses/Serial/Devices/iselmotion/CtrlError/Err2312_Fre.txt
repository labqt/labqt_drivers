;**************************************************************
;Error messages for CheckErr.DLL
;Do not modify this text file!
;(c) isel Automation KG 2002. All rights reserved.
;**************************************************************
;Facility: FAC_IFC5DLL

0-2312-0: Aucune erreur!

;//////////////////////////////////////////////////////////////
;Specific section

;Warnings
2-2312-1: Die �bertragungsgeschwindigkeit wurde automatisch auf die Voreinstellung der Steuerung umgestellt!\n\nBitte �berpr�fen Sie gelegentlich die Einstellungen f�r die serielle Verbindung und korrigieren Sie die Einstellungen f�r die �bertragungsgeschwindigkeit.

;Errors
3-2312-0: Aucune erreur!  
3-2312-1: Le nombre transf�r� est incorrect!
3-2312-2: Un fin de course  mat�riel est actuellement actif.  Une course de r�f�rence est exig�e!
3-2312-3: Sp�cifications d'axe invalide.  Peut-�tre un axe non initialis� a �t� consult�.
3-2312-4: Pas d'axes d�fini ! Peut-�tre le param�trage de la commande est incorrecte ou inachev�e.
3-2312-5: Erreur de syntax ! Une commande incorrecte, inachev�e ou inconnue a �t� transf�r�e � la commande.
3-2312-6: D�passement de m�moire ! La quantit� de donn�es transf�r�es exc�de la capacit� de la m�moire disponible.
3-2312-7: Nombre invalide de parametres!
3-2312-8: Commande CNC invalide! La commande transf�r�e n'existe pas en mode CNC.
3-2312-9: Erreur machine ! Soit l'alimentation est coup�e ou l'arrret d'urgence est enclench�.
3-2312-10: Code d'erreur non employ� par cette commande!
3-2312-11: Code d'erreur non employ� par cette commande!
3-2312-12: Code d'erreur non employ� par cette commande!
3-2312-13: La vitesse transf�r�e est invalide!
3-2312-14: Code d'erreur non employ� par cette commande!
3-2312-15: Stop utilisateur! Le bouton STOP a �t� press�, appuyer sur START pour continuer.
3-2312-16: Zone d'information incorrect!
3-2312-17: La porte est ouverte! La commande transf�r�e ne peut pas �tre ex�cut�e.
3-2312-18: Code d'erreur non employ� par cette commande!

3-2312-33: La course de r�f�rence n'a pas encore �t� effectuer!
3-2312-34: Au moins un axe se d�place!  Impossible d'ex�cuter la fonction demand�e.
3-2312-35: Valeur de priorit� invalide!
3-2312-36: Sp�cifications du plan de cercle invalide!
3-2312-37: Nombre de bit invalide! Impossible  ex�cuter l'op�ration d'E/S sur IFC5.
3-2312-38: Num�ro de port invalide! Impossible  ex�cuter l'op�ration d'E/S sur IFC5.
3-2312-39: Le port demand� est inactif! Impossible  ex�cuter l'op�ration d'E/S sur IFC5.
3-2312-41: Unterschiedliche Achsfaktoren! (Spindelsteigung, Getriebefaktoren, ...). Die gew�nschte Funktion kann nicht augef�hrt werden.
3-2312-42: Ung�ltige Kreisparameter! Einer oder mehrere Kreisparameter sind fehlerhaft, eine Kreisinterpolation ist mit diesen Parametern nicht m�glich.

3-2312-49: Ein Software-Endschalter ist momentan aktiv! Die Achse kann nur aus der Begrenzung herausgefahren werden.
3-2312-50: Fehlerhafte Konfiguration der Software-Endschalter! Der Wert des negativen Software-Endschalters ist gr��er als der Wert des positiven Software-Endschalters.


;//////////////////////////////////////////////////////////////
;Common Section

;Warnings

;Errors
3-2312-4098: Impossible de trouver les donn�es essentielles d'initialisation!
3-2312-4102: Le module MctlIFC5.DLL n'a pas �t� encore initialis�!
3-2312-4103: Le module MctlIFC5.DLL est occup� � ex�cuter une autre fonction!
3-2312-4369: Impossible de trouver le fichier d'initialisation!
3-2312-4370: Impossible d'ouvrir le fichier d'initialisation!
3-2312-4371: Impossible de lire le fichier d'initialisation!
3-2312-4372: Impossible d'�crire dans le fichier d'initialisation!
3-2312-4373: Version incorrect du fichier d'initialisation!
3-2312-4386: Impossible de cr�er/d'ouvrir un fichier temporaire!
3-2312-4387: Impossible de lire un fichier temporaire!
3-2312-4388: Impossible d'�crire dans un fichier temporaire!
3-2312-4401: Nom de fichier invalide!
3-2312-4609: Abandonn� par l'utilisateur!
3-2312-4610: Erreur mat�riel!
3-2312-4611: L'alimentation est coup�/Erreur alimentation!
3-2312-4625: Le module est bloqu� par le circuit de s�curit�!
3-2312-4641: Could not start a worker thread!
3-2312-4642: Une valeur d'index invalide a �t� utilis�e!
3-2312-4643: Des donn�es incorrectes ou invalide se sont produites!
3-2312-4644: Une position invalide a �t� donn� au control resp. Une erreur de position s'est produite!
3-2312-4645: Une variable de processus n'a pas pu �tre cr��e!
3-2312-4646: Function inconnue : L'une des fonctions demand�es est inconnue dans la DLL o� la version utilis�e est trop ancienne !
3-2312-4647: Pointer nul : Un pointeur invalide a �t� donn� !
3-2312-4648: Vitesse invalide : La valeur est soit n�gative, soit trop grande !
3-2312-4649: Vitesse invalide : La valeur est soit n�gative, soit trop grande !
3-2312-4650: Num�ro de port invalide : La valeur est soit n�gative, soit trop grande !
3-2312-4651: Nombre de bit invalide : La valeur est soit n�gative, soit sup�rieur � 7!
3-2312-4865: Erreur RS232! Impossible de se conncter � la carte d'interface.
3-2312-4866: Erreur RS232! Impossible d'ouvrir le port s�rie.
3-2312-4867: Erreur RS232! Impossible de d�finit les param�tres de communication.
3-2312-4868: Erreur RS232! Impossible d'activer le port s�rie!
3-2312-4869: Erreur RS232! Il manque la connection s�rie pour la carte interface IFC5.
3-2312-4870: Erreur RS232! Une erreur s'est produite pendant l'envoie des donn�es.
3-2312-4871: Erreur RS232! Une erreur s'est produite pendant la r�ception des donn�es.
3-2312-4872: Erreur RS232! Un timeout s'est produit pendant l'envoie des donn�es.
3-2312-5121: La porte de la machine est ouverte!
3-2312-5122: La porte de la machine est fermer!
3-2312-5137: L'axe est encore aliment�!
3-2312-5138: L'axe n'est plus aliment�!
3-2312-5139: Impossible de d�finir le sens de rotation d�sir� !
3-2312-5153: Les axes de la machines ne sont pas en position d'origine!
3-2312-5154: Les axes de la machines sont en position d'origine!
3-2312-5155: Les axes de la machines ne sont pas en position d'arret!
3-2312-5156: Les axes de la machines sont en position d'arret!
3-2312-65535: Erreur inconnue!


;//////////////////////////////////////////////////////////////
;End Of File

