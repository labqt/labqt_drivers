;**************************************************************
;Error messages for CheckErr.DLL
;Do not modify this text file!
;(c) isel Automation KG 1999. All rights reserved.
;**************************************************************
;Facility: FAC_IMC4DLL

0-2310-0: Kein Fehler!

;//////////////////////////////////////////////////////////////
;Specific section

;Warnings

;Errors
3-2310-0: No error!  
3-2310-1: The transferred number is erroneous!
3-2310-2: A hardware limit switch is currently active. A reference run is required!
3-2310-3: Invalid axis specification. Perhaps an uninitialized axis was accessed.
3-2310-4: No axes defined! Perhaps the control setup is erroneous or incomplete.
3-2310-5: Syntax error! An erroneous, incomplete or unknown command was transferred to the control.
3-2310-6: Memory overflow! The amount of transferred data exceeds the available memory size.
3-2310-7: Invalid number of parameters!
3-2310-8: Invalid CNC command! The transferred command does not exist in CNC mode.
3-2310-9: Machine error! Either the power supply is switched off or the Emergency Switch is pressed.
3-2310-10: Error code not used by this control!
3-2310-11: Error code not used by this control!
3-2310-12: CR expected: The interface card expected the character CR, but found another character. In most cases the number of given parameters is incorrect.
3-2310-13: The transferred speed is invalid!
3-2310-14: Error code not used by this control!
3-2310-15: User Stop! The STOP switch was pressed, press START to continue.
3-2310-16: Invalid data field!
3-2310-17: The cover is open! The transferred command can not be executed.
3-2310-18: Error code not used by this control!

3-2310-33: The reference run was not yet executed!
3-2310-34: At least one axis is moving!	Cannot execute the requested function.
3-2310-35: Invalid override value!
3-2310-36: Invalid circle plane specification!
3-2310-37: Invalid bit number! Cannot execute IMC4 IO operation. 
3-2310-38: Invalid port number! Cannot execute IMC4 IO operation.
3-2310-39: The requested port is inactive! Cannot execute IMC4 IO operation.
3-2310-40: Invalid helix parameter! (Angle is less than 0, Radius is less than 0, ...).
3-2310-41: Different axis factors! (Spindle elevation, gear factors, ...). The required function cannot be executed.
3-2310-42: Invalid circle parameter(s)! One or several circle parameters are incorrect, a circular interpolation is not possible with this parameters.
3-2310-43: Invalid acceleration! At least one value for the acceleration of a linear or rotative axis is invalid.
3-2310-44: Invalid start-/stop frequency! At least one value for the start-/stop frequency of a linear or rotative axis is invalid.

3-2310-49: A software limit switch is currently active! Axes can only be moved out of the software limit switch range.
3-2310-50: Configuration error! The value of the negative software limit switch is greater than the value of the positive software limit switch.

3-2310-65: Incorrect status condition of the control! Eventually, the STOP button was pressed during the execution of the reference run or the STOP button is not connected properly.


;//////////////////////////////////////////////////////////////
;Common Section

;Warnings

;Errors
3-2310-4098: Could not find essential initialization data!
3-2310-4102: The module MctlIFC5.DLL was not yet initialized!
3-2310-4103: The module MctlIFC5.DLL is busy executing another function!
3-2310-4369: Could not find the given initialization file!
3-2310-4370: Could not open the given initialization file!
3-2310-4371: Could not read from the given initialization file!
3-2310-4372: Could not write to the given initialization file!
3-2310-4373: Incorrect version of the given initialization file!
3-2310-4386: Could not create/open a temporary file!
3-2310-4387: Could not read from a temporary file!
3-2310-4388: Could not write to a temporary file!
3-2310-4401: Invalid file name!
3-2310-4609: Aborted by user!
3-2310-4610: Hardware error!
3-2310-4611: Power supply switched off / Power supply error!
3-2310-4612: The PC is too slow or there are not enough resources available!
3-2310-4625: Module is locked by security circuit!
3-2310-4641: Could not start a worker thread!
3-2310-4642: An invalid index value was used!
3-2310-4643: Erroneous or invalid data occured!
3-2310-4644: An invalid position was given to the control resp. erroneous positions occured!
3-2310-4645: A process variable could not be created!
3-2310-4646: Unknown function: Either the requested function is unknown within the module DLL or the version of the used DLL is too old!
3-2310-4647: NULL Pointer: A invalid pointer was given!
3-2310-4648: Invalid velocity: The given value is either negative or too big!
3-2310-4649: Invalid speed: The given value is either negative or too big!
3-2310-4650: Invalid port number: The given value is either negative or too big!
3-2310-4651: Invalid bit number: The given value is either negative or greater than 7!
3-2310-4865: RS232 error! Could not connect to interface card.
3-2310-4866: RS232 error! Could not open serial port.
3-2310-4867: RS232 error! Could not set communication parameters.
3-2310-4868: RS232 error! Could not enable serial port!
3-2310-4869: RS232 error! Missing serial connection to interface card IFC5.
3-2310-4870: RS232 error! An error occured while sending data.
3-2310-4871: RS232 error! An error occured while receiving data.
3-2310-4872: RS232 error! A timeout error occured during sending of data.
3-2310-5121: The cover of the machine is open!
3-2310-5122: The cover of the machine is closed!
3-2310-5137: The spindle is still switched on!
3-2310-5138: The spindle is switched off!
3-2310-5139: Could not set the requested turn direction!
3-2310-5153: The machines axes are not at their home position!
3-2310-5154: The machines axes are at their home position!
3-2310-5155: The machines axes are not at their park position!
3-2310-5156: The machines axes are at their park position!
3-2310-65535: Unknown error!

;//////////////////////////////////////////////////////////////
;End Of File

