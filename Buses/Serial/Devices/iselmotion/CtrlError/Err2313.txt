;**************************************************************
;Error messages for Motion Control IMS6
;Do not modify this text file!
;(c) isel Automation KG 2000. All rights reserved.
;**************************************************************
;Facility: FAC_IMS6DLL

0-2313-0: Kein Fehler!

;//////////////////////////////////////////////////////////////
; Comm Control

3-2313-1: Die serielle Schnittstelle kann nicht ge�ffnet werden !
3-2313-2: Einrichtung der Parameter der seriellen Schnittstelle fehlgeschlagen!
3-2313-3: Die serielle Schnittstelle kann nicht freigegeben werden!
3-2313-4: Serielle Verbindung fehlt!
3-2313-5: Fehler beim Senden von Daten!
3-2313-6: Fehler beim Empfang von Daten!
3-2313-7: Timeout-Fehler beim Betrieb der seriellen Schnittstelle!
3-2313-17: Die Comm Control wurde noch nicht initialisiert !
3-2313-18: Die Comm Control ist bereits mit einer weiteren Anforderung besch�ftigt!
3-2313-19: Die Comm Control konnte die Initialisierungsdatei nicht �ffnen!
3-2313-20: Die Comm Control konnte einen Arbeitsthread nicht starten !
3-2313-21: Der Comm Control wurden ungueltige Daten �bergeben (NULL_Ptr etc.)!

;//////////////////////////////////////////////////////////////
; Motion Control

3-2313-257: Die Motion Control konnte das Schnittstellenmodul nicht finden! 
3-2313-258: Die Motion Control konnte notwendige Initialisierungsdaten nicht finden!
3-2313-259: Die Motion Control konnte Initialisierungsdatei nicht �ffnen!
3-2313-260: Der Motion Control wurde ein zu alte Version des Schnittstellenmoduls �bergeben!
3-2313-261: Die Motion Control ist bereits mit einer weiteren Anforderung besch�ftigt!
3-2313-262: Die Motion Control wurde noch nicht initialisiert!
3-2313-263: Der Motion Control wurden ungueltige Daten �bergeben (NULL_Ptr etc.)!
3-2313-264: Der Motion Control wurden ungueltige Geschwindigkeitsangaben �bergeben!
3-2313-273: Die verwendete Funktion der Motion Control kann nur im Bahnmodus ausgef�hrt werden!
3-2313-274: Die verwendete Funktion der Motion Control kann nicht im Bahnmodus ausgef�hrt werden!


;//////////////////////////////////////////////////////////////
; IO Control

3-2313-513: Die IO Control konnte das Schnittstellenmodul nicht finden!
3-2313-514: Die IO Control konnte notwendige Initialisierungsdaten nicht finden!
3-2313-515: Die IO Control konnte Initialisierungsdatei nicht �ffnen!
3-2313-516: Der IO Control wurde ein zu alte Version des Comm Control �bergeben!
3-2313-517: Die IO Control ist bereits mit einer weiteren Anforderung besch�ftigt!
3-2313-518: Die IO Control wurde noch nicht initialisiert!
3-2313-519: Der IO Control wurden ungueltige Daten �bergeben (NULL_Ptr etc.)!
3-2313-529: Der IO Control wurde eine ungueltige Portnummer �bergeben!
3-2313-530: Der IO Control wurde eine ung�ltige Bitnummer �bergeben!
3-2313-531: Der IO Control wurde eine deaktivierte Portnummer �bergeben!
3-2313-531: Der IO Control wurde eine deaktivierte Bitnummer �bergeben! 

;//////////////////////////////////////////////////////////////
; Spindle Control

3-2313-769: Die Spindle Control konnte das Schnittstellenmodul nicht finden!
3-2313-770: Die Spindle Control konnte notwendige Initialisierungsdaten nicht finden!
3-2313-771: Die Spindle Control konnte Initialisierungsdatei nicht �ffnen!
3-2313-772: Der Spindle Control wurde ein zu alte Version des Comm Control �bergeben!
3-2313-773: Die Spindle Control ist bereits mit einer weiteren Anforderung besch�ftigt!
3-2313-774: Die Spindle Control wurde noch nicht initialisiert!
3-2313-775: Der Spindle Control wurden ungueltige Daten �bergeben (NULL_Ptr etc.)!
3-2313-776: Die verwendete Funktion der Spindle Control ist in dieser Version nicht vorhanden!

;//////////////////////////////////////////////////////////////
; Sec Control

3-2313-1025: Die SecC Control konnte das Schnittstellenmodul nicht finden!
3-2313-1026: Die SecC Control konnte notwendige Initialisierungsdaten nicht finden!
3-2313-1027: Die SecC Control konnte Initialisierungsdatei nicht �ffnen!
3-2313-1028: Der SecC Control wurde ein zu alte Version des Comm Control �bergeben!
3-2313-1029: Die SecC Control ist bereits mit einer weiteren Anforderung besch�ftigt!
3-2313-1030: Die SecC Control wurde noch nicht initialisiert!
3-2313-1031: Der SecC Control wurden ungueltige Daten �bergeben (NULL_Ptr etc.)!
3-2313-1032: Die SecC Funktion der SecC Control ist in dieser Version nicht vorhanden!

;//////////////////////////////////////////////////////////////
;End Of File