;**************************************************************
;Error messages for CheckErr.DLL
;Do not modify this text file!
;(c) isel Automation KG 1999. All rights reserved.
;**************************************************************


;//////////////////////////////////////////////////////////////
;Facility: FAC_SYSTEM

0-2049-0: Kein Fehler!
3-2049-1: Interner Windows Systemfehler!
3-2049-65535: Unbekannter Fehler!

;//////////////////////////////////////////////////////////////
;End Of File

