;**************************************************************
;Error messages for CheckErr.DLL
;Do not modify this text file!
;(c) isel Germany AG 04/2010. All rights reserved.
;**************************************************************
;Facility: FAC_INTERPRETER

;//////////////////////////////////////////////////////////////
;Specific section

0-2818-0: Aucune erreur, la derni�re op�ration a �t� ex�cut�e correctement.
3-2818-1: Impossible de trouver le fichier source %s!
3-2818-2: Impossible d'ouvrir le fichier source %s!
3-2818-3: Impossible d'�crire dans le fichier %s!

3-2818-17: Impossible de trouver le fichier CNC %s, la compilation du fichier source est requise!
3-2818-18: Impossible d'ouvrir le fichier CNC cible! Soit le nom du fichier donn�e est incorrect soit les sp�cifications de chemin sont invalides.
3-2818-19: Le fichier cible CNC n'est pas ouvert!
3-2818-20: Le fichier cible CNC est d�j� ouvert!
3-2818-21: Erreur de syntax dans le fichier NCP ou CNC!

3-2818-33: Impossible de trouver le fichier de sp�cifications label!
3-2818-34: Impossible d'ouvrir le fichier de sp�cifications label!
3-2818-35: Le fichier label n'est pas ouvert!
3-2818-36: Le fichier label est d�j� ouvert!
3-2818-37: Impossible de lire dans le fichier de sp�cifications label!

3-2818-49: Impossible de trouver le fichier log!
3-2818-50: Impossible d'ouvrir  le fichier log!
3-2818-51: Le fichier log n'est pas ouvert!
3-2818-52: Le fichier log est d�j� ouvert!
3-2818-53: Impossible d'�crire dans le fichier log!

3-2818-65: Impossible de trouver le fichier frame!
3-2818-66: Impossible d'ouvrir le fichier frame!
3-2818-67: Le fichier frame n'est pas ouvert!
3-2818-68: Le fichier frame est d�j� ouvert!
3-2818-69: Impossible d'�crire dans le fichier frame!

3-2818-81: Num�ro de ligne non trouv�! Le num�ro de ligne donn�e n'existe pas.
3-2818-82: Arret programme: L'ex�cution du fichier CNC par ordinateur a avort�...
3-2818-83: Optional Halt: L'execution du fichier CNC ne peut-�tre continu�...
3-2818-84: Un d�passement de s'est produit pendant le traitement des sous-programmes!
3-2818-85: Un d�passement de pile s'est produit pendant le traitement des sous-programmes
3-2818-86: Il manque le label %s (aucune cible d�finie dans un rapport goto)!
3-2818-87: Composant incorrect de frame : Le composant indiqu� de frame n'a pas pu �tre consult�.
3-2818-88: Impossible de trouver la frame %s dans le fichier frame %s.
3-2818-89: Index de variable P invalide, les valeurs d'index sont de 0 � 100
3-2818-90: Index de variable Q invalide, les valeurs d'index sont de 0 � 500
3-2818-91: Index de variable R invalide, les valeurs d'index sont de 0 � 1000
3-2818-92: Commande %s (logical program abortion) detect�!
3-2818-93: Fin de fichier NCP/CNC anormale dans  %s detect�: Arret du programme!
3-2818-94: Num�ro de port �ronn�, les num�ros valides sont de 1 � 8

3-2818-100: Le saut de programme est actif, mais impossible de trouver un fichier sautbut could not find a SKIP file which relates to the current CNC file!
3-2818-101: Le saut de programme est actif, but an error occured while opening the CNC file for file size detection!
3-2818-102: Le saut de programme est actif, but an error occured during CNC file size detection!
3-2818-103: Erreur de saut de programme: Impossible de lire la taille du dernier fichier CNC utilis� � partir du fichier sa rapportant au SAUT!
3-2818-104: Erreur de saut de programme: La taille du fichier CNC courant est diff�rente de celle du dernier fichier CNC utilis�!
3-2818-105: Erreur de saut de programme: Le fichier offset ne peut pas �tre lut depuis le fichier SAUT!
3-2818-106: Erreur de saut de programme: Une ou plusieur valeur ne peut-�tre lut depuis le fichier SAUT!
3-2818-107: Saut de programme est actif, mais configuration contradictoire d'axe du syst�me d'axe 1!
3-2818-108: Saut de programme est actif, mais configuration contradictoire d'axe du syst�me d'axe 2!
3-2818-109: Erreur de saut de programme: Mauvais point zero du syst�me d'axe 1!
3-2818-110: Erreur de saut de programme: La derni�re position arr�t d'axe du syst�me d'axe 1 est incorrecte!
3-2818-111: Erreur de saut de programme: Mauvais point zero du syst�me d'axe 2!
3-2818-112: Erreur de saut de programme: La derni�re position arr�t d'axe du syst�me d'axe 2 est incorrecte!
3-2818-113: Erreur de saut de programme: Impossible  trouver la date/heure du dernier fichier CNC pour �crire l'acc�s dans le fichier de SAUT!
3-2818-114: Erreur de saut de programme: A trouv� des rapports contradictoires de date/heure dans le  dernier acc�s du fichier CNC!
3-2818-115: Erreur de saut de programme: Le traitement du buffer de donn�es de tajectoire �tait inachev� au dernier avortement de programme!

3-2818-120: CYL-Befehl: die A-Achse im Achssystem 1 ist nicht als Rundachse konfiguriert! 
3-2818-121: CYL-Befehl: die B-Achse im Achssystem 1 ist nicht als Rundachse konfiguriert! 

3-2818-130: Instruction UserBat/UserExe: La taille de la liste d'argument exc�de 1024 byte.
3-2818-131: Instruction UserBat/UserExe: L'argument MODE n'est pas support� par le syst�me d'appel spawnlp().
3-2818-132: Instruction UserBat/UserExe: Un ou plusieur fichier(s) BATCH ou EXE sont introuvables
3-2818-133: Instruction UserBat/UserExe: Un ou plusieur fichier(s) BATCH ou EXE ne sont pas executable ou sont endomag�s
3-2818-134: Instruction UserBat/UserExe: Pas assez de m�moire pour execut� un fichier BATCH ou EXE.
3-2818-135: Instruction UserBat/UserExe: Le syst�me d'appel spawnlp() a d�fini ERRNO par un autre valeur qu'indiqu�e dans 130-134.
3-2818-136: Instruction UserDll dll_name dll_function: dll_name.dll ne peut-�tre trouv� dans ls r�pertoire de recherche donn�e.
3-2818-137: Instruction UserDll dll_name dll_function: dll_name.dll ne peut-�tre charger.
3-2818-138: Instruction UserDll dll_name dll_function: dll_function ne peut-�tre trouv� dans la DLL dll_name.

3-2818-140: Index du syst�me d'axe invalide (Valeur valide : 0 pour le system d'axe 1 et 1 pour le syst�me d'axe 2)!
3-2818-141: im Befehl wurde ein Adre�buchstabe f�r eine Achse benutzt, die nicht initialisiert und damit nicht verf�gbar ist

3-2818-150: La frame %s aurait d�t �tre cr�er, mais elle existe d�j� dans le fichier frame %s. 
3-2818-151: La structure du fichier frame %s est endomag�!
3-2818-152: Le fichier frame %s est vide!
3-2818-153: Erreur d'allocation m�moire!
3-2818-154: Le nom de la frame %s est invalide.

3-2818-160: Au moins un axe de machine ne peut pas ex�cuter un mouvement ou une erreur dans un amplificateur de puissance!
3-2818-161: Mouvement d'axe(s) impossible: Arr�t mouvement actif (BREAK) 
3-2818-162: Mouvement d'axe(s) impossible: Un fin de course mat�riel est actif 
3-2818-163: Motion of axis/axes impossible: Un fin de course logiciel est actif
3-2818-164: Un fin de course mat�riel a �t� activ� pendant le mouvement
3-2818-165: Mouvement d'axe(s) impossible: Follow error / drag error in at least one axis  
3-2818-166: Mouvement d'axe(s) impossible: Motor amplifiers without operating power (POWER FAIL)
3-2818-167: Mouvement d'axe(s) impossible: Erreur signal� par le module de s�curtit� (SC) (SC FAIL)
3-2818-168: Mouvement d'axe(s) impossible: Hardware error in Motor Controller / Motor amplifiers / Motor inverters
3-2818-169: Mouvement d'axe(s) impossible: Erreur de comunication avec le controleur / amplificateur de puissance
3-2818-170: Erreur critique dans le module de puissance: Erreur de mouvement, ou defaut de puissance sur un amplificateur

3-2818-180: Mouvement d'arr�t de tout axes: Erreur critique dans l'inverseur d'axe (Temp�rature trop �lev�e / Surintensit� / Panne de courant)

3-2818-190: Mouvement d'arr�t de tout axes: Erreur critique sur au moins une entr�e/sortie

3-2818-200: invalid row index when reading from / writing to an Excel file *.xls
3-2818-201: invalid column index when reading from / writing to an Excel file *.xls

3-2818-240: No USB Stick (WIBU-Box lite) with isel Firm Code found at any USB port: Program execution only without assignation of Motion Control (MCTL) supported

3-2818-250: Index p�riph�rique invalidex: La valeur d'index est %s!

;//////////////////////////////////////////////////////////////
;Common Section

3-2818-4097: DLL non trouv�!
3-2818-4098: Impossible de trouver les donn�es essentielles d'initialisation de l'interpreteur CNC!
3-2818-4099: Conflit de version ! Le module interpreteur CNC ne peut pas executer la fonction demand�e!
3-2818-4100: Index de module invalide!
3-2818-4101: Le module interpreteur CNC est d�sactiv�!
3-2818-4102: Le module interpreteur CNC n'est pas encore initialis�!
3-2818-4103: Le module est occup� paur un autre appel de fonction!
3-2818-4353: Fichier non trouv�!
3-2818-4354: Impossible d'ouvrir le fichier!
3-2818-4355: Impossible de lire le fichier!
3-2818-4356: Impossible d'�crire dans le fichier!
3-2818-4369: Impossible de trouver le fichier d'initialisation de l'interpreteur CNC!
3-2818-4370: Impossible d'ouvrir le fichier d'initialisation de l'interpreteur CNC!
3-2818-4371: Impossible de lire le fichier d'initialisation de l'interpreteur CNC!
3-2818-4372: Impossible d'�crire dans le fichier d'initialisation de l'interpreteur CNC!
3-2818-4373: Version incorrect du fichier d'initialisation de l'interpreteur CNC!
3-2818-4386: Un fichier temporaire ne peut-�tre ouvert!
3-2818-4387: Impossible de lire dans un fichier temporaire!
3-2818-4388: Impossible d'�crire dans un fichier temporaire!
3-2818-4401: Nom de fichier invalide!
3-2818-4609: Annuler par l'utilisateur! La derni�re action ex�cut�e a �t� avort�e � la demande de l'utilisateur.
3-2818-4610: Erreur mat�riel!
3-2818-4611: Erreur d'alimentation!
3-2818-4612: L'ordinateur est trop lent!
3-2818-4625: L'op�ration est bloqu� par le circuit de s�curit�!
3-2818-4641: Impossible de d�marer un thread!
3-2818-4642: Une valeur d'index invalide a �t� utilis�e!
3-2818-4643: Des donn�es incorrectes ou invalide se sont produites!
3-2818-4644: Une position invalide a �t� donn� au control resp. Une erreur de position s'est produite!
3-2818-4645: Une variable de processus n'a pas pu �tre cr��e!
3-2818-4646: Function inconnue : L'une des fonctions demand�es est inconnue dans la DLL o� la version utilis�e est trop ancienne !
3-2818-4647: Pointer nul : Un pointeur invalide a �t� donn� !
3-2818-4648: Vitesse invalide : La valeur est soit n�gative, soit trop grande !
3-2818-4649: Vitesse invalide : La valeur est soit n�gative, soit trop grande !
3-2818-4650: Num�ro de port invalide : La valeur est soit n�gative, soit trop grande !
3-2818-4651: Nombre de bit invalide : La valeur est soit n�gative, soit sup�rieur � 7!
3-2818-4865: Erreur RS232! Impossible d'�tablir la connectio s�rie.
3-2818-4866: Erreur RS232! Impossible d'ouvrir le port s�rie.
3-2818-4867: Erreur RS232! Impossible de d�finir les param�tres de communication.
3-2818-4868: Erreur RS232! Impossible d'activer le port s�rie!
3-2818-4869: Erreur RS232! Il manque la connection s�rie.
3-2818-4870: Erreur RS232! Une erreur s'est produite pendant l'envoie des donn�es.
3-2818-4871: Erreur RS232! Une erreur s'est produite pendant la r�ception des donn�es.
3-2818-4872: Erreur RS232! Un timeout s'est produit pendant l'envoie des donn�es.
3-2818-5121: La porte de la machine est ouverte!
3-2818-5122: La porte de la machine est ferm�!
3-2818-5137: L'axe est encore aliment�!
3-2818-5138: L'axe n'est plus aliment�!
3-2818-5139: Impossible de d�finir le sens de rotation d�sir�!
3-2818-5153: Les axes de la machines ne sont pas en position d'origine!
3-2818-5154: Les axes de la machines sont en position d'origine!
3-2818-5155: Les axes de la machines ne sont pas en position d'arret!
3-2818-5156: Les axes de la machines sont en position d'arret!
3-2818-65535: Erreur inconnue!

;//////////////////////////////////////////////////////////////
;End Of File

