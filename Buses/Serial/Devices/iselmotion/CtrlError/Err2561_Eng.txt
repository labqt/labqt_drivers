;**************************************************************
;Error messages for CheckErr.DLL
;Do not modify this text file!
;(c) isel Automation KG 1999. All rights reserved.
;**************************************************************
;Facility: FAC_SPINDLE

0-2561-0: No error!

;//////////////////////////////////////////////////////////////
;Specific section

;Errors
3-2561-1: Invalid spindle speed! The given speed cannot be set by this spindle control.
3-2561-2: External error signal (Tool clamp not closed or air pressure missing) at Frequency converter active! Spindle must not be switched on. 
3-2561-3: Invalid minimum or maximum speed! The spindle parameters include erroneous value(s) for the minimum resp. maximum speed. Eventually the values are interchanged?
3-2561-17: Access to IO environment not possible! The IO control is not yet initialized or a serious error occured, may a version conflict is the cause?
3-2561-33: The external error input #1 is active. The spindle cannot be started.
3-2561-34: The external error input #2 is active. The spindle cannot be started.
3-2561-35: External error: Air pressure too low! The spindle cannot be started.
3-2561-36: External error: The collet is open! The spindle cannot be started.
3-2561-37: External error: The tool holder is not drawn into the collet completely! The spindle cannot be started.


;//////////////////////////////////////////////////////////////
;Common Section

;Warnings
2-2561-4101: The spindle module is disabled! The requested function cannot be executed.

;Errors
3-2561-4097: DLL not found!
3-2561-4098: Could not find essential initialization data of this spindle module!
3-2561-4099: Version conflict! The spindle module does not provide the requested function.
3-2561-4100: Invalid module index! The requested spindle module does not exist.
3-2561-4101: The selected spindle module is disabled!
3-2561-4102: The selected spindle module is not yet initialized!
3-2561-4103: The spindle module is busy with another function call!
3-2561-4353: File not found!
3-2561-4354: Could not open file!
3-2561-4355: Could not read from file!
3-2561-4356: Could not write to file!
3-2561-4369: Could not find the spindle module's initialization file!
3-2561-4370: Could not open the spindle module's initialization file!
3-2561-4371: Could not read from the spindle module's initialization file!
3-2561-4372: Could not write to the spindle module's initialization file!
3-2561-4373: Incorrect version of the spindle module's initialization file!
3-2561-4386: A temporary file could not be opened!
3-2561-4387: Could not read from a temporary file!  
3-2561-4388: Could not write to a temporary file!   
3-2561-4401: Invalid file name!
3-2561-4609: User break! The last executed action was aborted by user request.
3-2561-4610: Hardware error!
3-2561-4611: Power fail!
3-2561-4625: The spindle is locked by the security circuit module!
3-2561-4641: Could not start a worker thread!
3-2561-4642: An invalid index value was used!
3-2561-4643: Erroneous or invalid data occured!
3-2561-4644: An invalid position was given to the control resp. erroneous positions occured!
3-2561-4645: A process variable could not be created!
3-2561-4646: Unknown function: Either the requested function is unknown within the module DLL or the version of the used DLL is too old!
3-2561-4647: NULL Pointer: A invalid pointer was given!
3-2561-4648: Invalid velocity: The given value is either negative or too big!
3-2561-4649: Invalid speed: The given value is either negative or too big!
3-2561-4650: Invalid port number: The given value is either negative or too big!
3-2561-4651: Invalid bit number: The given value is either negative or greater than 7!
3-2561-4865: RS232 error: Could not establish the serial connection to the frequency converter!
3-2561-4866: RS232 error: Could not open the serial port!
3-2561-4867: RS232 error: Could not set the parameters for serial transmission!
3-2561-4868: RS232 error: Could not release the serial port!
3-2561-4869: RS232 error: Missing serial connection to the frequency converter!
3-2561-4870: RS232 error: Error during sending of data!
3-2561-4871: RS232 error: Error during receiving of data!
3-2561-4872: RS232 error: Timeout error during RS232 communication!
3-2561-5121: The cover of the machine is open!
3-2561-5122: The cover of the machine is closed!
3-2561-5137: The spindle is still switched on!
3-2561-5138: The spindle is switched off!
3-2561-5139: Could not set the requested turn direction!
3-2561-5153: The machines axes are not at their home position!
3-2561-5154: The machines axes are at their home position!
3-2561-5155: The machines axes are not at their park position!
3-2561-5156: The machines axes are at their park position!
3-2561-65535: Unknown error!

;//////////////////////////////////////////////////////////////
;End Of File
