RSRC
 LVCCLBVW  X  $      8   � �0          < � @�      ����            �����.@�&Zf�	�          �(r����K�	ȹ��}��ُ ��	���B~       M��BJ�F�.���)��   ��ZXM��?��7n-o�   �  11.0.1            & LVCCFPGA_I2C IO Cluster.ctl            8   4Specifies the FPGA IO items for the I2C port to use.   ������  �  �@ �� !�� a�  !�� !�� !�� !�� !�� �� �� �� �� �� �� �� ��0��,�����$���.9��$�� ���,��0�@ �  �  ����   ���������������������������������                              ��                              ��        +                     ��       +��               �    ��      +����             ��    ��   ��V                   �    ��       ���               �    ��       ���               �    ��       ���               �    ��       ���               �    ��       ��� ��                 ��   ��V ���Vu�                 ��      +�P&Ju�+                ��     V��Jutu�+                ��      ����uv�+                ��      �����ʬ+                ��       ����V�+                ��   ��V ���  ++                ��       ���       ��+          ��       Ь�       ���+        ��       ���     ##���+      ��       ���       ����+    ��       ���       �����##  ��   V�V ���       ����+    ��               �����+      ��      +����      ���+        ��       +��       ��+          ��        +                     ��                              ��                              ���������������������������������   & FPHPFPGA_I2C IO Cluster.ctl           � �               displayFilter �                   tdData �               IOInterface �     0����     C<Interface>
<MethodSet>
   <Method name="Read">
      <AttributeSet>
         <Attribute name="NumberOfSyncRegistersForRead">
            <LocalizedName>Number of Synchronizing Registers for Read</LocalizedName>
            <LocalizedValues>Auto,0,1,2</LocalizedValues>
            <SupportedValues>InheritFromProjectItem,Auto,0,1,2</SupportedValues>
            <Value>Auto</Value>
         </Attribute>
      </AttributeSet>
      <ReturnValue>
         <Type>bool</Type>
      </ReturnValue>
   </Method>
   <Method name="Set Output Data">
      <ParameterList>
         <Parameter name="Data">
            <Direction>in</Direction>
            <Required>yes</Required>
            <Type>bool</Type>
         </Parameter>
      </ParameterList>
      <ReturnValue>
         <Type>void</Type>
      </ReturnValue>
   </Method>
   <Method name="Set Output Enable">
      <ParameterList>
         <Parameter name="Enable">
            <Direction>in</Direction>
            <Required>yes</Required>
            <Type>bool</Type>
         </Parameter>
      </ParameterList>
      <ReturnValue>
         <Type>void</Type>
      </ReturnValue>
   </Method>
   <Method name="Write">
      <ParameterList>
         <Parameter name="Value">
            <Type>bool</Type>
         </Parameter>
      </ParameterList>
   </Method>
</MethodSet></Interface>       	typeClass �     0����      FPGA I/O       � �               displayFilter �                   tdData �               IOInterface �     0����     C<Interface>
<MethodSet>
   <Method name="Read">
      <AttributeSet>
         <Attribute name="NumberOfSyncRegistersForRead">
            <LocalizedName>Number of Synchronizing Registers for Read</LocalizedName>
            <LocalizedValues>Auto,0,1,2</LocalizedValues>
            <SupportedValues>InheritFromProjectItem,Auto,0,1,2</SupportedValues>
            <Value>Auto</Value>
         </Attribute>
      </AttributeSet>
      <ReturnValue>
         <Type>bool</Type>
      </ReturnValue>
   </Method>
   <Method name="Set Output Data">
      <ParameterList>
         <Parameter name="Data">
            <Direction>in</Direction>
            <Required>yes</Required>
            <Type>bool</Type>
         </Parameter>
      </ParameterList>
      <ReturnValue>
         <Type>void</Type>
      </ReturnValue>
   </Method>
   <Method name="Set Output Enable">
      <ParameterList>
         <Parameter name="Enable">
            <Direction>in</Direction>
            <Required>yes</Required>
            <Type>bool</Type>
         </Parameter>
      </ParameterList>
      <ReturnValue>
         <Type>void</Type>
      </ReturnValue>
   </Method>
   <Method name="Write">
      <ParameterList>
         <Parameter name="Value">
            <Type>bool</Type>
         </Parameter>
      </ParameterList>
   </Method>
</MethodSet></Interface>       	typeClass �     0����      FPGA I/O       M  Jx��V�oSU�������ܹsl����9�"!*���n��1��t �6��Y�Y/�,7�i�݃	ѧ���_����Dc"�4��I3w{��so{/�V��6�=�=����s{ ��N��V��IºI 
I
����E�g��+�I�����y���*5H7�=����)lA�nv(Cj��_w���JHv��,�����u#�����W%I�@�nY6�#��I*�JҨ��ifsN->�1��"�4GJ��	����<�����#�8�R�����"fZ-����Hs�E��b��v��>�����q[��
�:�).cA�Z<F���~'�|����9ji�r�2�Y{[��Da)\�,x�0��A�:�)�>4��"dP���x<�>�M.��'���s�Hjf� ud�d����faqq[ū݇��X�)�2�[���͈g�f�<�(�$&@�k�"�V�O��@_�ÞL�LV[�2}a�e�"�g�F���3p���^r@�Lq?����D�5�L�A�Շ����rl�Lje�r�G�2!�X������%���=CFHV�1��0����}�^�!AC�BéMeJ��h}��KU���_���3F�ڦc�+*h�-{�U��Q�k��J�Z<�YP����@�9͸c��gp��h(�f�܂�nv�PM>c�	��L�V�tm���xu:1��Ԕ��r؍�`�{�9g62�AwA�E��@W!���F_�|����i�)~ӭ<��[���h�43��'N˼���\ᨴ��ި�<���������ӽ��e43ڧ[�Z�3f��8��������\�j�;���[���o���̟-����	&�`b\Z�a�����X\7~4����sҡ����������H.{咞�M������LO]����%��	C8�d�g4�a�(��}H��-���#ňkGG�ntʑGtʱmsJ����━�w���:��-�r�?pJ���v9eh�4`�a�_"�����UiJ���	�� �����^�0�5vh����?e�t����%��MZ*�R=�NV��m�BJ�"Fw�*�!�         <   & BDHPFPGA_I2C IO Cluster.ctl            e   ux�c``(�`��P���I�+�!���YЏ�7���a �( 	����.��>��� �l���9�2-���W)b��z�\�8Se�<� e�            1      NI.LV.All.SourceOnly �     !                     h   (                                        �                    �     @UUA N 	                                                   ��*>  �>��*>  �>     @   ?          �  Zx��T���0�[�ZN Wk�YVh��h����eS�+8�4�n�R;�6R��p�8�	�_ ��:M���,��{��ge ����~����S ���]�\�Fq�$�Y���t�Ȓ,���"8�_���l�28 ��!a���1�mޔB)�*�p3_���b�"�MV,�%]�ÖC.jj%���IN�0;��`�$|Fw.8�W�]�Bf\��{u��o�\�2�佣ޓ����ՙU\HW���C�#��c*#��޶`�R�Iۀ��[�y�:g���e�R	v��*0�r����:�&������1$V�P��T�=J�h�ϴ�ۧ�B�P����+�Y@��UQ5�J��`�K�t�A7\��ʵR]}gۿ5�#�ٿ1�K�9�����3���nc�o��ח������{��4��3�I���tב�9���Ұ7鿂�?���~|�����?2�w�q_3H?Wf>>NeOG�ҼV����k��i��~��ظ�      e       H      � �   Q      � �   Z      � �   c� � �   � �Segoe UISegoe UISegoe UI0   RSRC
 LVCCLBVW  X  $      8               4     LVSR      RTSG       vers      4CONP      HLIvi      \STRG      pICON      �icl8      �LIfp      �STR      �FPHb      �FPSE      �LIbd      BDHb      $BDSE      8VITS      LDTHP      `MUID      tHIST      �PRT       �VCTP      �FTAB      �                        ����       �       ����       �        ����       �        ����       �        ����       �        ����      $        ����      �        ����      �       ����      �       ����      �        ����      �        ����      ,        ����      4        ����      `        ����      �        ����      �        ����              ����              ����              ����      H        ����      �       �����      �    FPGA_I2C IO Cluster.ctl