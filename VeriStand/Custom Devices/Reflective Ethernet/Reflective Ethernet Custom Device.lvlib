﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="11008008">
	<Property Name="NI.Lib.Icon" Type="Bin">%1#!#!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(]!!!*Q(C=\&gt;7R=2MR%!81N=?"5Q&lt;/07RB7W!,&lt;'&amp;&lt;9+K1,7Q,&lt;)%N&lt;!NMA3X)DW?-RJ(JQ"I\%%Z,(@`BA#==ZB3RN;]28_,V7@PWW`:R`&gt;HV*SU_WE@\N_XF[3:^^TX\+2YP)D7K6;G-RV3P)R`ZS%=_]J'XP/5N&lt;XH,7V\SEJ?]Z#5P?=J4HP+5JTTFWS%0?=B$DD1G(R/.1==!IT.+D)`B':\B'2Z@9XC':XC':XBUC?%:HO%:HO&amp;R7QT0]!T0]!S0I4&lt;*&lt;)?=:XA-(]X40-X40-VDSGC?"GC4N9(&lt;)"D2,L;4ZGG?ZH%;T&gt;-]T&gt;-]T?.S.%`T.%`T.)^&lt;NF8J4@-YZ$S'C?)JHO)JHO)R&gt;"20]220]230[;*YCK=ASI2F=)1I.Z5/Z5PR&amp;)^@54T&amp;5TT&amp;5TQO&lt;5_INJ6Z;"[(H#&gt;ZEC&gt;ZEC&gt;Z$"(*ETT*ETT*9^B)HO2*HO2*(F.&amp;]C20]C2)GN4UE1:,.[:/+5A?0^NOS?UJ^3&lt;*\9B9GT@7JISVW7*NIFC&lt;)^:$D`5Q9TWE7)M@;V&amp;D,6;M29DVR]6#R],%GC47T9_/=@&gt;Z5V&gt;V57&gt;V5E&gt;V5(OV?^T[FTP?\`?YX7ZRP6\D=LH%_8S/U_E5R_-R$I&gt;$\0@\W/VW&lt;[_"\Y[X&amp;],0^^+,]T_J&gt;`J@_B_]'_.T`$KO.@I"XC-_N!!!!!!</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Item Name="AcceptIncoming.vi" Type="VI" URL="../AcceptIncoming.vi"/>
	<Item Name="ActionVIOnDeleteInputAndOutput.vi" Type="VI" URL="../ActionVIOnDeleteInputAndOutput.vi"/>
	<Item Name="ActionVIOnDeleteRequestInputAndOutput.vi" Type="VI" URL="../ActionVIOnDeleteRequestInputAndOutput.vi"/>
	<Item Name="AddChannel.vi" Type="VI" URL="../AddChannel.vi"/>
	<Item Name="AddTarget.vi" Type="VI" URL="../AddTarget.vi"/>
	<Item Name="AsynchHandleIncoming.vi" Type="VI" URL="../AsynchHandleIncoming.vi"/>
	<Item Name="AsynchHandleOutgoing.vi" Type="VI" URL="../AsynchHandleOutgoing.vi"/>
	<Item Name="Checksum.vi" Type="VI" URL="../Checksum.vi"/>
	<Item Name="ConnectOutgoing.vi" Type="VI" URL="../ConnectOutgoing.vi"/>
	<Item Name="DataFormatting.vi" Type="VI" URL="../DataFormatting.vi"/>
	<Item Name="FunctionGlobalInput.vi" Type="VI" URL="../FunctionGlobalInput.vi"/>
	<Item Name="FunctionGlobalMode.ctl" Type="VI" URL="../FunctionGlobalMode.ctl"/>
	<Item Name="FunctionGlobalOutput.vi" Type="VI" URL="../FunctionGlobalOutput.vi"/>
	<Item Name="InitiateFunctionGlobals.vi" Type="VI" URL="../InitiateFunctionGlobals.vi"/>
	<Item Name="NetworkParameters.ctl" Type="VI" URL="../NetworkParameters.ctl"/>
	<Item Name="Page 1.vi" Type="VI" URL="../Page 1.vi"/>
	<Item Name="Page 2.vi" Type="VI" URL="../Page 2.vi"/>
	<Item Name="Page 3.vi" Type="VI" URL="../Page 3.vi"/>
	<Item Name="Page 4.vi" Type="VI" URL="../Page 4.vi"/>
	<Item Name="Post-Build Action.vi" Type="VI" URL="../Post-Build Action.vi"/>
	<Item Name="Reflective Ethernet GUID Lookup.vi" Type="VI" URL="../Reflective Ethernet GUID Lookup.vi"/>
	<Item Name="Reflective Ethernet Initialization VI.vi" Type="VI" URL="../Reflective Ethernet Initialization VI.vi"/>
	<Item Name="Reflective Ethernet Main Page.vi" Type="VI" URL="../Reflective Ethernet Main Page.vi"/>
	<Item Name="Reflective Ethernet RT Driver VI.vi" Type="VI" URL="../Reflective Ethernet RT Driver VI.vi"/>
	<Item Name="SetupIncomingRef.vi" Type="VI" URL="../SetupIncomingRef.vi"/>
	<Item Name="SetupOutgoingRef.vi" Type="VI" URL="../SetupOutgoingRef.vi"/>
	<Item Name="TCPCleanUp.vi" Type="VI" URL="../TCPCleanUp.vi"/>
	<Item Name="TCPConnectWrapper.vi" Type="VI" URL="../TCPConnectWrapper.vi"/>
	<Item Name="TCPListenWrapper.vi" Type="VI" URL="../TCPListenWrapper.vi"/>
</Library>
